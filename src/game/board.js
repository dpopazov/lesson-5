const User = require('./user');
const Cell = require('./cell');
const Leaderboard = require('./leaderboard');
const {
  DIVIDER,
  GAME_DESCRIPTION1,
  GAME_DESCRIPTION2,
  PLAYER1_TURN,
  PLAYER2_TURN,
  GOOD_LUCK,
  VALID_VALUES,
  VALID_VALUE_MESSAGE,
  PLAYER1_VALUE,
  PLAYER2_VALUE,
  CONGRATS,
  WON,
  NO_WINNER
} = require('../constants');
const { ask } = require('../utils/ask');

class Board {

  constructor({ player1, player2 }, rl) {

    this.matrix = [
      [new Cell(1), new Cell(2), new Cell(3)],
      [new Cell(4), new Cell(5), new Cell(6)],
      [new Cell(7), new Cell(8), new Cell(9)],
    ];

    this.player1 = new User(player1);
    this.player2 = new User(player2);
    this.rl = rl;
    this.turn = 0;
    this.leaderboard = new Leaderboard();
  }

  outputBoardView = () => {

    const topCoordinates = this.matrix[0].reduce((acc, item) => `${acc}    ${item.position}   `, '');
    const bottomCoordinates = this.matrix[2].reduce((acc, item) => `${acc}    ${item.position}   `, '');

    console.clear();
    this.rl.write(`${topCoordinates}\n`);

    this.matrix.forEach((item, index) => {
      const row = item.reduce((acc, { position, value }, idx) => {

        let resultStr = '';
        if (idx === 0) {
          resultStr = `${position}   ${value || ' '}   |`;
        } else if (idx === item.length - 1) {
          resultStr = `${acc}   ${value || ' '}   ${position}`;
        } else {
          resultStr = `${acc}   ${value || ' '}   |`
        }

        return resultStr;
      }, '');

      this.rl.write(`${row}\n`);

      if (index !== this.matrix.length - 1) {
        this.rl.write(`${DIVIDER}\n`);
      }
    });

    this.rl.write(`${bottomCoordinates}\n`);
  }

  runGame = () => {

    this.outputBoardView();
    this.rl.write(`${GAME_DESCRIPTION1}\n`);
    this.rl.write(`${GAME_DESCRIPTION2}\n`);
    this.rl.write(`${GOOD_LUCK}\n`);

    this.nextTurn();
  }

  inputValidationRule = data => {
    const numberData = parseInt(data, 10);
    let isCellAlreadyInUse;

    if (Number.isNaN(numberData)) {
      return false;
    } else if (!VALID_VALUES.includes(numberData)) {
      return false;
    }

    this.matrix.forEach(item => {
      item.forEach(elem => {
        if (elem.position === numberData) {
          isCellAlreadyInUse = elem.value;
        }
      });
    });

    return !isCellAlreadyInUse;
  }

  nextTurn = async () => {

    let input;
    const isPlayer1Turn = !this.turn || this.turn % 2 === 0;
    const playerValue = isPlayer1Turn ? PLAYER1_VALUE : PLAYER2_VALUE;

    if (isPlayer1Turn) {
      input = await ask(`${PLAYER1_TURN}${this.player1.name} `, this.inputValidationRule, VALID_VALUE_MESSAGE);
    } else {
      input = await ask(`${PLAYER2_TURN}${this.player2.name} `, this.inputValidationRule, VALID_VALUE_MESSAGE);
    }

    ++this.turn;

    this.setMatrix(parseInt(input), playerValue);
    this.outputBoardView();
    const isWin = this.checkWinCombination(playerValue);
    const noTurns = !this.checkFreeCells();

    if (isWin) {
      const winnerName = isPlayer1Turn ? this.player1.name : this.player2.name;
      this.rl.write(`${CONGRATS} ${winnerName} ${WON}\n`);

      this.leaderboard.saveStats(winnerName);
      const stats = this.leaderboard.getStats();

      this.rl.write(`${stats}\n`);

      this.rl.close();
    } else if (noTurns) {
      this.rl.write(`${NO_WINNER}\n`);
      this.rl.close();
    } else {
      this.nextTurn();
    }
  }

  setMatrix = (cellNumber, value) => {
    this.matrix.forEach(item => item.forEach(elem => elem.position === cellNumber && elem.setValue(value)));
  }

  checkWinCombination = playerValue => {

    const rowWin =
      !this.matrix[0].some(({ value }) => value !== playerValue) ||
      !this.matrix[1].some(({ value }) => value !== playerValue) ||
      !this.matrix[2].some(({ value }) => value !== playerValue);

    if (rowWin) {
      return rowWin;
    }

    const firstColumn = [this.matrix[0][0], this.matrix[1][0], this.matrix[2][0]];
    const secondColumn = [this.matrix[0][1], this.matrix[1][1], this.matrix[2][1]];
    const thirdColumn = [this.matrix[0][2], this.matrix[1][2], this.matrix[2][2]];

    const columnWin =
      !firstColumn.some(({ value }) => value !== playerValue) ||
      !secondColumn.some(({ value }) => value !== playerValue) ||
      !thirdColumn.some(({ value }) => value !== playerValue);

    if (columnWin) {
      return columnWin;
    }

    const firstDiagonal = [this.matrix[0][0], this.matrix[1][1], this.matrix[2][2]];
    const secondDiagonal = [this.matrix[0][2], this.matrix[1][1], this.matrix[2][0]];

    const diagonalWin =
      !firstDiagonal.some(({ value }) => value !== playerValue) ||
      !secondDiagonal.some(({ value }) => value !== playerValue);

    if (diagonalWin) {
      return diagonalWin;
    }

    return false;
  }

  checkFreeCells = () => {
    const boolMatrix = this.matrix.map(item => item.some(({ value }) => value === null));

    return boolMatrix.includes(true);
  }
}

module.exports = Board;
