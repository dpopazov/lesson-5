const fs = require('fs');
const path = require('path');

class Leaderboard {

  saveStats = username => {
    fs.appendFileSync(path.join(__dirname, '../stats/stats.csv'), `\n${username}`);
  }

  getStats = () => fs.readFileSync(path.join(__dirname, '../stats/stats.csv'), 'utf8');
}

module.exports = Leaderboard;
