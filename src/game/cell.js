class Cell {
  constructor(position) {
    this.position = position;
    this.value = null;
  }

  setValue = value => this.value = value;
}

module.exports = Cell;
