const Board = require('./board');
const {
  FILL_NAME_MESSAGE,
  PLAYER1_NAME_MESSAGE,
  PLAYER2_NAME_MESSAGE
} = require('../constants');
const { ask, rl } = require('../utils/ask');

const bootstrap = async () => {

  const player1 = await ask(PLAYER1_NAME_MESSAGE, null, FILL_NAME_MESSAGE);
  const player2 = await ask(PLAYER2_NAME_MESSAGE, null, FILL_NAME_MESSAGE);

  const data = {
    player1,
    player2,
  };

  const board = new Board(data, rl);

  board.runGame();
};

bootstrap();
