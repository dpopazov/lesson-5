const DIVIDER = '   -------------------   ';
const FILL_NAME_MESSAGE = 'Please, fill out the name ';
const CORRECT_DATA_MESSAGE = 'Enter correct data ';
const PLAYER1_NAME_MESSAGE = 'Enter Player 1 name ';
const PLAYER2_NAME_MESSAGE = 'Enter Player 2 name ';
const PLAYER1_TURN = 'Player 1 ';
const PLAYER2_TURN = 'Player 2 ';
const PLAYER1_VALUE = 'X';
const PLAYER2_VALUE = 'O';
const GAME_DESCRIPTION1 = 'Number near cell is number which you should input when your turn.';
const GAME_DESCRIPTION2 = 'The middle cell`s number is 5.';
const GOOD_LUCK = 'Good luck!';
const VALID_VALUES = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const VALID_VALUE_MESSAGE = 'Entered value should be from 1 to 9 and cell should not be in use. ';
const CONGRATS = 'Congratulations!';
const WON = 'won!';
const NO_WINNER = 'No winner! Try again.';

module.exports = {
  DIVIDER,
  FILL_NAME_MESSAGE,
  CORRECT_DATA_MESSAGE,
  PLAYER1_NAME_MESSAGE,
  PLAYER2_NAME_MESSAGE,
  GAME_DESCRIPTION1,
  GAME_DESCRIPTION2,
  PLAYER1_TURN,
  PLAYER2_TURN,
  GOOD_LUCK,
  VALID_VALUES,
  VALID_VALUE_MESSAGE,
  PLAYER1_VALUE,
  PLAYER2_VALUE,
  CONGRATS,
  WON,
  NO_WINNER
};
