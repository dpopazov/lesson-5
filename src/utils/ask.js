const readline = require('readline');
const { CORRECT_DATA_MESSAGE } = require('../constants');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const ask = (question, validation, validationMessage) => {
  return new Promise(res => {
    rl.question(question, async data => {

      let secondaryResponse;
      if (!data.length || (typeof validation === 'function' && !validation(data))) {
        secondaryResponse = await ask(validationMessage || CORRECT_DATA_MESSAGE , validation, validationMessage);
      }

      res(secondaryResponse || data);
    });
  });
};

module.exports = { ask, rl };
